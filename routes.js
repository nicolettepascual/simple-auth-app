const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

const mongoose = require('mongoose');
const User = require('./user');

const bcrypt = require('bcryptjs');

const checkAuth = require('./check-auth');

router.post('/register', (req, res, next) => {
  let hasErrors = false ;
  let errors = [];
  
  if(!req.body.name){
  //validate name presence in the request
    errors.push({'name': 'Name not received'})
    hasErrors = true;
  }
  if(!req.body.email){
    //validate email presence in the request
    errors.push({'email': 'Email not received'})
    hasErrors = true;
  }
  if(!req.body.password){
    //validate password presence in the request
    errors.push({'password': 'Password not received'})
    hasErrors = true;
  }

if(hasErrors){
    //if there is any missing field
    res.status(401).json({
      message: "Invalid input",
      errors: errors
    });

  }else{
    //encrypt user password
    bcrypt.hash(req.body.password, 10, (err, hashed_password) => { 
      if(err){
        errors.push({
          hash: err.message
        });
        return res.status(500).json(errors);
      }else{
        //if password is hashed
        const new_user = new User({
          _id : mongoose.Types.ObjectId(),
          name: req.body.name,
          email: req.body.email,
          password: hashed_password
        });
        //save in the database
        new_user.save().then(saved_user => {
          res.status(201).json({
            message: 'User registered',
            user: saved_user,
            errors: errors
          });
        }).catch(err => {
          errors.push(new Error({
            db: err.message
          }))
          res.status(500).json(errors);
        })
      }
    });
  }

});

router.post('/login', (req, res, next) => {
  let hasErrors = false ;
  let errors = [];

  if(!req.body.email){
    errors.push({'email': 'Email not received'})
    hasErrors = true;
  }
  if(!req.body.password){
    errors.push({'password': 'Password not received'})
    hasErrors = true;
  }

  if(hasErrors){
    res.status(422).json({
      message: "Invalid input",
      errors: errors
    });

  }else{
  //check if credentials are valid
    User.findOne({'email': req.body.email}).then((found_user, err) => {
      if(!found_user){
        //if user is not registered
        res.status(401).json({
          message: "Auth error, email not found"
        });
      }else{
        //validate password
        bcrypt.compare(req.body.password, found_user.password, (err, isValid) => {
          if(err){
            res.status(500).json({
              message: err.message
            }) 
          }
          if(!isValid){ //incorrect password
            res.status(401).json({
              message: "Auth error"
            }) 
          }else{
            const token = jwt.sign(
              {
                email: req.body.email, 
              }, 
              process.env.JWT_KEY, 
              {
                expiresIn: "1h"
              }
            );
            //validation OK
            res.status(200).json({
              message: 'Auth OK',
              token: token,
              errors: errors
            })
          }
        });
      }
    });
  
  }
});

router.get('/protected', checkAuth, (req, res, next)=> {
  res.status(200).json({
    message: 'Welcome, your email is ' + req.userData.email,
    user: req.userData,
    errors: [],
  })
})


module.exports = router;