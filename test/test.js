const chai = require('chai');
const expect = chai.expect;

const app = require('../app');

const http = require('chai-http');
chai.use(http);

const User = require('../user')

//App basics test
describe('App', () => {

  before( (done) => {
    //delete all users 
    User.find().deleteMany().then( res => {
      console.log('Users removed');
    }).catch(err => {
      console.log(err.message);
    });
	done();
  });

  it('should exist', () => {
    expect(app).to.be.a('function');
  })
  //test if app gets HTTP GET request in '/'
  it('should return 200 and message and get HTTP GET request in /', (done) => {
    chai.request(app).get('/')
      .then((res) => {
        expect(res).to.have.status(200);
        expect(res.body.message).to.contain('GET in');
        done();
    }).catch(err => {
      console.log(err.message);
    })
  });
})

describe('User registration', () => {
  it('should return 201 and confirm valid input', (done) => {
    const new_user = {
      "name"  : "User Person",
      "email": "user@person.com",
      "password": "password"
    }
    chai.request(app).post('/register')
      .send(new_user)
        .then((res) => {
		  //console.log(res.body);
          expect(res).to.have.status(201);
          expect(res.body.message).to.be.equal("User registered");
          
		  expect(res.body.user._id).to.exist;
		  expect(res.body.user.createdAt).to.exist;
		  
		  expect(res.body.user.password).to.not.be.eql(new_user.password);
		  
        }).catch(err => {
          console.log(err.message);
        })
	done();	
  });

})

describe('User login', () => {
    it('should return 200 and token for valid credentials', (done) => {
      const valid_input = {
        "email": "user@person.com",
        "password": "password"
      }
      chai.request(app).post('/login')
        .send(valid_input)
          .then((res) => {
            expect(res).to.have.status(200);
            expect(res.body.token).to.exist;
            expect(res.body.message).to.be.equal("Auth OK");
            expect(res.body.errors.length).to.be.equal(0);
            
          }).catch(err => {
            console.log(err.message);
          })
		done();
    });
  });
  
describe('Protected route', () => {

  it('should return 200 and user details if valid token provided', (done) => {
    //mock login to get token
    const valid_input = {
      "email": "user@person.com",
      "password": "password"
    }
    //send login request to the app to receive token
    chai.request(app).post('/login').send(valid_input).then((login_response) => {
          //add token to next request Authorization headers as Bearer adw3R£$4wF43F3waf4G34fwf3wc232!w1C"3F3VR
          const token = 'Bearer ' + login_response.body.token;
          chai.request(app).get('/protected').set('Authorization', token).then(protected_response => {
              expect(protected_response).to.have.status(200);
              expect(protected_response.body.message).to.be.equal('Welcome, your email is user@person.com');
              expect(protected_response.body.user.email).to.exist;
              expect(protected_response.body.errors.length).to.be.equal(0);
            }).catch(err => {
              console.log(err.message);
            });
        }).catch(err => {
          console.log(err.message);
        });
		done();
  })

	//stopping server after tests
    after((done) => {
    console.log('All tests completed, stopping server....')
    process.exit();
    done();
  });  

});